# set up path before everything else
fish_add_path ~/.local/bin
fish_add_path ~/.npm-global/bin
fish_add_path ~/.cargo/bin

# easy cd
abbr ... ../..
abbr .... ../../..
abbr ..... ../../../..

# environment
set -gx EDITOR vi

# aliases
abbr l ls
abbr la ls -a
abbr cat_npm_scripts jq .scripts package.json

## gpg
set SSH_AUTH_SOCK (gpgconf --list-dirs agent-ssh-socket)
set GPG_TTY (tty)
set GPG_EMAIL "didyouknowthat@dafne.rocks"
gpgconf --launch gpg-agent

abbr encrypt_for_self gpg --sign --encrypt -u $GPG_EMAIL -r $GPG_EMAIL

## git
abbr gf git fetch -p
abbr gc git commit -v
abbr gcda git commit -v --date=\"(date)\"
abbr gb git branch
abbr gbD git branch -D
abbr gd git diff
abbr gds git diff --staged
abbr gm git merge
abbr gl git pull
abbr glrb git pull --rebase
abbr gst git status
abbr gco git checkout
abbr grb git rebase
abbr grba git rebase --abort
abbr grbc git rebase --continue
abbr grbi git rebase --interactive
abbr gcp git cherry-pick
abbr gcpa git cherry-pick --abort
abbr gcpc git cherry-pick --continue
abbr glog git log
abbr glgo git log --graph --oneline
abbr gp git push
abbr gpf git push --force-with-lease

## tmux
abbr ts tmux new -s
abbr tl tmux ls
abbr ta tmux attach -t

### wip
abbr wip git commit --no-verify -m '"--wip-- [ci skip]"'
function unwip
    set -l last_commit (git log -1 --format=%B)
    if string match -q -- '--wip--*' -- $last_commit
        git reset --soft HEAD~
    else
        echo 'Current HEAD not a WIP commit'
    end
end

## get external ips, https://unix.stackexchange.com/a/81699/37512
abbr wanip 'dig @resolver4.opendns.com myip.opendns.com +short'
abbr wanip4 'dig @resolver4.opendns.com myip.opendns.com +short -4'
abbr wanip6 'dig @resolver1.ipv6-sandbox.opendns.com AAAA myip.opendns.com +short'

# system-specific

## homebrew setup, https://brew.sh
if test -d /opt/homebrew/bin
    fish_add_path /opt/homebrew/bin

    # include some keg-only thingies
    fish_add_path /opt/homebrew/opt/ruby/bin

    # pyenv
    alias brew="env PATH=(string replace (pyenv root)/shims '' \"\$PATH\") brew"
end

## gcloud completion/cask
## $ set -Ux CLOUDSDK_HOME ...
if set -q CLOUDSDK_HOME && test -d $CLOUDSDK_HOME
    source "$CLOUDSDK_HOME/path.fish.inc"
end

## ngrok completion
if command -q ngrok
    ngrok completion | source
end

## rich-editor setup
if type -q hx
    set -gx EDITOR hx
else if type -q helix
    set -gx EDITOR helix
    abbr hx helix
else if type -q nvim
    set -gx EDITOR nvim
    abbr vim nvim
end

## set up xdg-open alias (or open for macOS)
if type -q xdg-open
    abbr x xdg-open
else if type -q open
    abbr x open
end

## nnn setup
if type -q nnn
    set -gx NNN_BMS 'w:~/Workspaces;d:~/Documents;D:~/Downloads;p:~/Pictures;m:~/Music;v:~/Videos'

    # optionally enable trash
    if type -q trash-put
        set -gx NNN_TRASH 1
    end
end

## clipboard setup
switch (uname)
    case Linux
        if test "$XDG_SESSION_TYPE" = wayland && type -q wl-copy && type -q wl-paste
            set -gx COPY wl-copy -n
            set -gx PASTE wl-paste -n
        else if test "$XDG_SESSION_TYPE" = x11 && type -q xclip
            set -gx COPY xclip -selection clipboard -rmlastnl
            set -gx PASTE xclip -selection clipboard -out
        end
    case Darwin
        set -gx COPY pbcopy
        set -gx PASTE pbpaste
end

## use eza instead of ls if available
if type -q eza
    abbr l eza
    abbr ls eza
    abbr la eza -a
end

## alias fd to fdfind if available
if type -q fdfind
    abbr fd fdfind
end

## fuzzy search commands
if type -q fzy
    function fs
        fd -H -t file 2>/dev/null | fzy | $COPY
    end

    function hs
        history | fzy | eval "$COPY"
    end

    function v
        if test -z $argv
            set -l pasted ($PASTE)
            if test -e $pasted
                $EDITOR $pasted
            else if git rev-parse --verify $pasted &>/dev/null
                git checkout $pasted
            else
                fs
            end
        else
            $EDITOR $argv
        end
    end
end

## tz setup, https://github.com/oz/tz
if type -q tz
    set -gx TZ_LIST 'Etc/UTC,Europe/Amsterdam,Asia/Tokyo,Australia/Sydney,America/New_York,America/Chicago,America/Los_Angeles'
    abbr tzq tz -q
end

## direnv setup
if type -q direnv
    direnv hook fish | source
end

## tj/n
if type -q n
    set -gx N_PREFIX "$HOME/.local/share/"
    fish_add_path $N_PREFIX/bin
end

## asdf
if test -d ~/.asdf
    source ~/.asdf/asdf.fish

    ## automatically create fish completions
    if not test -f ~/.config/fish/completions/asdf.fish
        mkdir -p ~/.config/fish/completions; and ln -s ~/.asdf/completions/asdf.fish ~/.config/fish/completions
    end
end

## android studio setup on macos
if test -d $HOME/Library/Android/sdk
    set ANDROID_HOME $HOME/Library/Android/sdk
    set ANDROID_SDK_ROOT $ANDROID_HOME
    set PATH $ANDROID_HOME/emulator $PATH
    set PATH $ANDROID_HOME/tools $PATH
    set PATH $ANDROID_HOME/tools/bin $PATH
    set PATH $ANDROID_HOME/platform-tools $PATH
end

## rbenv
if type -q rbenv
    status --is-interactive; and rbenv init - fish | source
end

## pyenv
if type -q pyenv
    set -Ux PYENV_ROOT $HOME/.pyenv
    fish_add_path $PYENV_ROOT/bin
    pyenv init - | source
end

## imagemagick aliases and functions
function image-size
    set path_to_image $argv[1]

    if ! test -f $path_to_image
        return 1
    end

    set image_size (du -h $path_to_image | cut -f1)
    set image_dimensions (identify -ping -format '%wx%h' $path_to_image)

    echo $path_to_image $image_size $image_dimensions
end

## vscodium
if type -q codium
    abbr code codium
end

## yazi
function y
    set tmp (mktemp -t "yazi-cwd.XXXXXX")
    yazi $argv --cwd-file="$tmp"
    if set cwd (command cat -- "$tmp"); and [ -n "$cwd" ]; and [ "$cwd" != "$PWD" ]
        builtin cd -- "$cwd"
    end
    rm -f -- "$tmp"
end

# private scripts and environment variables setup
if test -f $HOME/.config/fish/private.fish
    source $HOME/.config/fish/private.fish
end

# prompt setup
if type -q starship
    starship init fish | source
end
