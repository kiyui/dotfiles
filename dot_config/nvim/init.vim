" #region dein.vim setup:
let $CACHE = expand('~/.cache')
if !($CACHE->isdirectory())
  call mkdir($CACHE, 'p')
endif
if &runtimepath !~# '/dein.vim'
  let s:dir = 'dein.vim'->fnamemodify(':p')
  if !(s:dir->isdirectory())
    let s:dir = $CACHE .. '/dein/repos/github.com/Shougo/dein.vim'
    if !(s:dir->isdirectory())
      execute '!git clone https://github.com/Shougo/dein.vim' s:dir
    endif
  endif
  execute 'set runtimepath^='
        \ .. s:dir->fnamemodify(':p')->substitute('[/\\]$', '', '')
endif

if &compatible
  set nocompatible
endif

" Add the dein installation directory into runtimepath
set runtimepath+=~/.cache/dein/repos/github.com/Shougo/dein.vim

if dein#load_state('~/.cache/dein')
  call dein#begin('~/.cache/dein')
  call dein#add('~/.cache/dein/repos/github.com/Shougo/dein.vim')

  call dein#add('tpope/vim-repeat')          " Make . powerful!
  call dein#add('tpope/vim-surround')        " Vim bracket/parentheses wrapping
  call dein#add('tpope/vim-fugitive')        " Vim plugin for Git

  if !exists('g:vscode')
    call dein#add('dense-analysis/ale')      " Syntax, lint, & LSP
    call dein#add('mcchrish/nnn.vim')        " File browser
    call dein#add('cloudhead/neovim-fuzzy')  " Fuzzy search
    call dein#add('sheerun/vim-polyglot')    " Syntax support
    call dein#add('delphinus/vim-firestore') " Firebase rules syntax
    call dein#add('github/copilot.vim')      " GitHub Copilot
    call dein#add('mfussenegger/nvim-dap')   " Node.js debugger
    call dein#add('luochen1990/rainbow')     " Flower power
  endif

  if !has('nvim')
    call dein#add('roxma/nvim-yarp')
    call dein#add('roxma/vim-hug-neovim-rpc')
  endif

  call dein#end()
  call dein#save_state()
endif

filetype plugin indent on
syntax enable
" #endregion dein.vim setup

" #region plugin-specific setup
" dense-analysis/ale
let g:ale_fixers = {
\   'javascript': ['eslint', 'prettier'],
\   'javascriptreact': ['eslint', 'prettier'],
\   'typescript': ['eslint', 'prettier'],
\   'typescriptreact': ['eslint', 'prettier'],
\}
let g:ale_completion_enabled = 1

nnoremap <F2> :ALERename
nnoremap <F12> :ALEGoToDefinition -tab<CR>

" mcchrish/nnn.vim
let g:nnn#set_default_mappings = 0
let g:nnn#command = 'NNN_TRASH=1 nnn -H'
let g:nnn#action = { '<c-t>': 'tab split', '<c-h>': 'split', '<c-v>': 'vsplit' }
let g:nnn#layout = { 'window': { 'width': 0.9, 'height': 0.6, 'highlight': 'Debug' } }
nnoremap <leader>n :call nnn#pick(expand('%:p:h'), { 'edit': 'vertical split' })<cr>

" cloudhead/neovim-fuzzy
nnoremap <C-p> :FuzzyOpen<CR>

let g:fuzzy_hidden = 1
let g:fuzzy_bindkeys = 0

autocmd FileType fuzzy tnoremap <silent> <buffer> <Enter> <C-\><C-n>:FuzzyOpenFileInVSplit<CR>
autocmd FileType fuzzy tnoremap <silent> <buffer> <C-T> <C-\><C-n>:FuzzyOpenFileInTab<CR>
autocmd FileType fuzzy tnoremap <silent> <buffer> <C-S> <C-\><C-n>:FuzzyOpenFileInSplit<CR>
autocmd FileType fuzzy tnoremap <silent> <buffer> <C-V> <C-\><C-n>:FuzzyOpenFileInVSplit<CR>

" github/copilot.vim
let g:copilot#enable = 1

" luochen1990/rainbow
let g:rainbow_active = 1
" #endregion plugin-specific setup

" #region personal settings
set number                 " show line numbers
set list                   " show hidden characters
set breakindent            " ident wrapped lines
set incsearch              " jump to match as searching
set ignorecase             " case-insensitive search by default
set smartcase              " case-sensitive search when an uppercase char is used
set mouse=a                " support all the mouse
set clipboard+=unnamedplus " system clipboard
set autoread               " automagically update file contents

" esc is too far
imap jk <Esc>

" move lines up and down
nnoremap - ddkP
nnoremap _ ddp

" show column
set colorcolumn=80,100,120 " show columns
highlight ColorColumn ctermbg=yellow guibg=yellow

" clear search highlight on double escape without clearing register
nnoremap <esc><esc> :noh<return>

" base64 encode/decode
vnoremap <leader>de64 y:echo system('base64 --decode', @")<cr>
vnoremap <leader>d64 c<c-r>=system('base64 --decode', @")<cr><esc>
vnoremap <leader>e64 c<c-r>=system('base64', @")<cr><esc>

" i use c# comments for code regions
hi regionComment ctermbg=black
call matchadd('regionComment', '.*#\s*\(end\)\=region.*$')

autocmd Filetype *
      \ let b:match_words = '\s*#\s*region.*$:\s*#\s*endregion'
" #endregion personal settings

" #region file-specific setup
autocmd Filetype markdown
	\ setlocal spell spelllang=en_gb

autocmd Filetype markdown,html,vue
	\ setlocal iskeyword+=-

autocmd Filetype python
	\ setlocal tabstop=4 |
	\ setlocal shiftwidth=4 |
	\ setlocal softtabstop=4 |
	\ setlocal expandtab

autocmd Filetype javascript,typescript,svelte
	\ setlocal tabstop=2 |
	\ setlocal shiftwidth=2 |
	\ setlocal softtabstop=2 |
	\ setlocal expandtab
" #endregion file-specific setup

" #region project specific configuration
set exrc
set secure
" #endregion project specific configuration

" #region zellij
function! SwitchZellijMode(mode)
  if exists('$ZELLIJ')
    call jobstart(["zellij", "action", "switch-mode", a:mode], {'detach': v:true})
    " call system(["zellij", "action", "switch-mode", a:mode])
  endif
endfunction

autocmd VimEnter * call SwitchZellijMode("locked")
autocmd VimLeave * call SwitchZellijMode("normal")
" #endregion zellij
